# HTML i CSS - Snippety
> Kilka ważnych informacji

Przed przystąpieniem do rozwiązywania zadań przeczytaj poniższe wskazówki

### 1. Jak zrobić prosty formularz logowania?
Zwróć uwagę jakie pola zostały wykorzystane do jego stworznia.

```html
<form action="" class="form">
    <fieldset>
        <legend>Logowanie</legend>

        <input type="email" placeholder="Email">
        <input type="password" placeholder="Password">

        <label for="remember-me">
            <input id="remember-me" type="checkbox"> Zapamiętaj mnie
        </label>

        <button type="submit" class="button"></button>
    </fieldset>
</form>
```

### 2. Wyśrodkowanie elementu w pionie i poziomie wewnątrz innego elementu

```html
<div class="one">
    <div class="two"></div>
</div>
```

```css
.one {
    background-color: blue;
    height: 100px;
    position: relative;
    width: 100px;
}

.two {
    background-color: red;
    bottom: 0;
    height: 50px;
    left: 0;
    margin: auto;
    position: absolute;
    right: 0;
    top: 0;
    width: 50px;
}
```

Więcej informacji znajdziesz [pod tym linkiem](https://www.smashingmagazine.com/2013/08/absolute-horizontal-vertical-centering-css/).

### 3. Prosty clearfix

```css
.clearfix:before, .clearfix:after {
    clear: both;
    content: "";
    display: table;
}
```

Więcej informacji znajdziesz [pod tym linkiem](http://stackoverflow.com/questions/8554043/what-is-a-clearfix).
